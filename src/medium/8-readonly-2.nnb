{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/8-medium-readonly-2/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/8-medium-readonly-2/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>Readonly 2 <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23readonly-999\" alt=\"#readonly\"/> <img src=\"https://img.shields.io/badge/-%23object--keys-999\" alt=\"#object-keys\"/></h1><blockquote><p>by Anthony Fu <a href=\"https://github.com/antfu\" target=\"_blank\">@antfu</a></p></blockquote><p><a href=\"https://tsch.js.org/8/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> &nbsp;&nbsp;&nbsp;<a href=\"./README.zh-CN.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87-gray\" alt=\"简体中文\"/></a>  <a href=\"./README.ja.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E6%97%A5%E6%9C%AC%E8%AA%9E-gray\" alt=\"日本語\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement a generic `MyReadonly2<T, K>` which takes two type argument `T` and `K`.",
                                "",
                                "`K` specify the set of properties of `T` that should set to Readonly. When `K` is not provided, it should make all properties readonly just like the normal `Readonly<T>`.",
                                "",
                                "For example",
                                "",
                                "```ts",
                                "interface Todo {",
                                "  title: string",
                                "  description: string",
                                "  completed: boolean",
                                "}",
                                "",
                                "const todo: MyReadonly2<Todo, 'title' | 'description'> = {",
                                "  title: \"Hey\",",
                                "  description: \"foobar\",",
                                "  completed: false,",
                                "}",
                                "",
                                "todo.title = \"Hello\" // Error: cannot reassign a readonly property",
                                "todo.description = \"barFoo\" // Error: cannot reassign a readonly property",
                                "todo.completed = true // OK",
                                "```",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/8/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/8/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <hr><h3>Related Challenges</h3><a href=\"https://github.com/type-challenges/type-challenges/blob/master/questions/7-easy-readonly/README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-7%E3%83%BBReadonly-7aad0c\" alt=\"7・Readonly\"/></a>  <a href=\"https://github.com/type-challenges/type-challenges/blob/master/questions/9-medium-deep-readonly/README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-9%E3%83%BBDeep%20Readonly-d9901a\" alt=\"9・Deep Readonly\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/8-medium-readonly-2/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype MyReadonly2<Type, Keys extends keyof Type = keyof Type> =\n  Type & { readonly [Key in Keys]: Type[Key] }\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Alike, Expect } from '@type-challenges/utils'\n\ntype Cases = [\n  Expect<Alike<MyReadonly2<Todo1>, Readonly<Todo1>>>,\n  Expect<Alike<MyReadonly2<Todo1, 'title' | 'description'>, Expected>>,\n  Expect<Alike<MyReadonly2<Todo2, 'title' | 'description'>, Expected>>,\n]\n\ninterface Todo1 {\n  title: string\n  description?: string\n  completed: boolean\n}\n\ninterface Todo2 {\n  readonly title: string\n  description?: string\n  completed: boolean\n}\n\ninterface Expected {\n  readonly title: string\n  readonly description?: string\n  completed: boolean\n}\n"
            ],
            "outputs": []
        }
    ]
}