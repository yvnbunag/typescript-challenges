{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/2693-medium-endswith/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/2693-medium-endswith/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>EndsWith <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23template--literal-999\" alt=\"#template-literal\"/></h1><blockquote><p>by jiangshan <a href=\"https://github.com/jiangshanmeta\" target=\"_blank\">@jiangshanmeta</a></p></blockquote><p><a href=\"https://tsch.js.org/2693/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement `EndsWith<T, U>` which takes two exact string types and returns whether `T` ends with `U`",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/2693/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/2693/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/2693-medium-endswith/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype EndsWith<\n  Type extends string,\n  Substring extends string\n> = Type extends `${string}${Substring}` ? true : false\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect, ExpectFalse, NotEqual } from '@type-challenges/utils'\n\ntype Cases = [\n  Expect<Equal<EndsWith<'abc', 'bc'>, true>>,\n  Expect<Equal<EndsWith<'abc', 'abc'>, true>>,\n  Expect<Equal<EndsWith<'abc', 'd'>, false>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}