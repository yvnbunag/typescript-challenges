{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/531-medium-string-to-union/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/531-medium-string-to-union/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>String to Union <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23union-999\" alt=\"#union\"/> <img src=\"https://img.shields.io/badge/-%23string-999\" alt=\"#string\"/></h1><blockquote><p>by Andrey Krasovsky <a href=\"https://github.com/bre30kra69cs\" target=\"_blank\">@bre30kra69cs</a></p></blockquote><p><a href=\"https://tsch.js.org/531/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> &nbsp;&nbsp;&nbsp;<a href=\"./README.ja.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E6%97%A5%E6%9C%AC%E8%AA%9E-gray\" alt=\"日本語\"/></a> </p><!--info-header-end-->\r",
                                "\r",
                                "Implement the String to Union type. Type take string argument. The output should be a union of input letters\r",
                                "\r",
                                "For example\r",
                                "\r",
                                "```ts\r",
                                "type Test = '123';\r",
                                "type Result = StringToUnion<Test>; // expected to be \"1\" | \"2\" | \"3\"\r",
                                "```\r",
                                "\r",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/531/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/531/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->\r",
                                ""
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/531-medium-string-to-union/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype StringToUnion<\n  Type extends string\n> = Type extends `${infer Current}${infer Rest}`\n  ? Current | StringToUnion<Rest>\n  : never\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ntype Cases = [\n  Expect<Equal<StringToUnion<\"\">, never>>,\n  Expect<Equal<StringToUnion<\"t\">, \"t\">>,\n  Expect<Equal<StringToUnion<\"hello\">, \"h\" | \"e\" | \"l\" | \"l\" | \"o\">>,\n  Expect<Equal<StringToUnion<\"coronavirus\">, \"c\" | \"o\" | \"r\" | \"o\" | \"n\" | \"a\" | \"v\" | \"i\" | \"r\" | \"u\" | \"s\">>,\n]\n"
            ],
            "outputs": []
        }
    ]
}