{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/2852-medium-omitbytype/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/2852-medium-omitbytype/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>OmitByType <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23object-999\" alt=\"#object\"/></h1><blockquote><p>by jiangshan <a href=\"https://github.com/jiangshanmeta\" target=\"_blank\">@jiangshanmeta</a></p></blockquote><p><a href=\"https://tsch.js.org/2852/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->\r",
                                "\r",
                                "From ```T```, pick a set of properties whose type are not assignable to ```U```.\r",
                                "\r",
                                "For Example\r",
                                "\r",
                                "```typescript\r",
                                "type OmitBoolean = OmitByType<{\r",
                                "  name: string\r",
                                "  count: number\r",
                                "  isReadonly: boolean\r",
                                "  isEnable: boolean\r",
                                "}, boolean> // { name: string; count: number }\r",
                                "```\r",
                                "\r",
                                "\r",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/2852/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/2852/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->\r",
                                ""
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/2852-medium-omitbytype/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype OmitByType<Type, OmittedType> = {\n  [Key in keyof Type as Type[Key] extends OmittedType ? never : Key]: Type[Key]\n}\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ninterface Model {\n  name: string\n  count: number\n  isReadonly: boolean\n  isEnable: boolean\n}\n\ntype Cases = [\n  Expect<Equal<OmitByType<Model, boolean>, { name: string; count: number }>>,\n  Expect<Equal<OmitByType<Model, string>, { count: number; isReadonly: boolean; isEnable: boolean }>>,\n  Expect<Equal<OmitByType<Model, number>, { name: string; isReadonly: boolean; isEnable: boolean }>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}