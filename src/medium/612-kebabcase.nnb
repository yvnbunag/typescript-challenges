{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/612-medium-kebabcase/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/612-medium-kebabcase/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>KebabCase <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23template--literal-999\" alt=\"#template-literal\"/></h1><blockquote><p>by Johnson Chu <a href=\"https://github.com/johnsoncodehk\" target=\"_blank\">@johnsoncodehk</a></p></blockquote><p><a href=\"https://tsch.js.org/612/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "`FooBarBaz` -> `for-bar-baz`",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/612/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/612/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/612-medium-kebabcase/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype LetterMap =\n  { upper: 'A' , lower: 'a'} |\n  { upper: 'B' , lower: 'b'} |\n  { upper: 'C' , lower: 'c'} |\n  { upper: 'D' , lower: 'd'} |\n  { upper: 'E' , lower: 'e'} |\n  { upper: 'F' , lower: 'f'} |\n  { upper: 'G' , lower: 'g'} |\n  { upper: 'H' , lower: 'h'} |\n  { upper: 'I' , lower: 'i'} |\n  { upper: 'J' , lower: 'j'} |\n  { upper: 'K' , lower: 'k'} |\n  { upper: 'L' , lower: 'l'} |\n  { upper: 'M' , lower: 'm'} |\n  { upper: 'N' , lower: 'n'} |\n  { upper: 'O' , lower: 'o'} |\n  { upper: 'P' , lower: 'p'} |\n  { upper: 'Q' , lower: 'q'} |\n  { upper: 'R' , lower: 'r'} |\n  { upper: 'S' , lower: 's'} |\n  { upper: 'T' , lower: 't'} |\n  { upper: 'U' , lower: 'u'} |\n  { upper: 'V' , lower: 'v'} |\n  { upper: 'W' , lower: 'w'} |\n  { upper: 'X' , lower: 'x'} |\n  { upper: 'Y' , lower: 'y'} |\n  { upper: 'Z' , lower: 'z'}\n\n  type KebabCase<\n    Type extends string\n  > = Type extends `${infer First}${infer OuterRest}`\n      ? OuterRest extends `${infer Second}${infer Rest}`\n        ? Second extends LetterMap['upper']\n          ? `${Lowercase<First>}-${KebabCase<`${Lowercase<Second>}${Rest}`>}`\n          : `${Lowercase<First>}${KebabCase<OuterRest>}`\n        : `${Lowercase<First>}${KebabCase<OuterRest>}`\n      : Type\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ntype Cases = [\n  Expect<Equal<KebabCase<'FooBarBaz'>, 'foo-bar-baz'>>,\n  Expect<Equal<KebabCase<'fooBarBaz'>, 'foo-bar-baz'>>,\n  Expect<Equal<KebabCase<'foo-bar'>, 'foo-bar'>>,\n  Expect<Equal<KebabCase<'foo_bar'>, 'foo_bar'>>,\n  Expect<Equal<KebabCase<'Foo-Bar'>, 'foo--bar'>>,\n  Expect<Equal<KebabCase<'ABC'>, 'a-b-c'>>,\n  Expect<Equal<KebabCase<'-'>, '-'>>,\n  Expect<Equal<KebabCase<''>, ''>>,\n  Expect<Equal<KebabCase<'😎'>, '😎'>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}