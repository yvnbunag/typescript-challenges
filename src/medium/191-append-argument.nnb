{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/191-medium-append-argument/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/191-medium-append-argument/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>Append Argument <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23arguments-999\" alt=\"#arguments\"/></h1><blockquote><p>by Maciej Sikora <a href=\"https://github.com/maciejsikora\" target=\"_blank\">@maciejsikora</a></p></blockquote><p><a href=\"https://tsch.js.org/191/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> &nbsp;&nbsp;&nbsp;<a href=\"./README.zh-CN.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87-gray\" alt=\"简体中文\"/></a>  <a href=\"./README.ja.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E6%97%A5%E6%9C%AC%E8%AA%9E-gray\" alt=\"日本語\"/></a> </p><!--info-header-end-->",
                                "",
                                "For given function type `Fn`, and any type `A` (any in this context means we don't restrict the type, and I don't have in mind any type 😉) create a generic type which will take `Fn` as the first argument, `A` as the second, and will produce function type `G` which will be the same as `Fn` but with appended argument `A` as a last one.",
                                "",
                                "For example,",
                                "",
                                "```typescript",
                                "type Fn = (a: number, b: string) => number",
                                "",
                                "type Result = AppendArgument<Fn, boolean> ",
                                "// expected be (a: number, b: string, x: boolean) => number",
                                "```",
                                "",
                                "> This question is ported from the [original article](https://dev.to/macsikora/advanced-typescript-exercises-question-4-495c) by [@maciejsikora](https://github.com/maciejsikora)",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/191/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/191/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->",
                                ""
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/191-medium-append-argument/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype AppendArgument<\n  Function extends (...args: Array<any>)=> any,\n  ExtraParameter\n> = (...args: [...Parameters<Function>, ExtraParameter]) => ReturnType<Function>\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ntype Case1 = AppendArgument<(a: number, b: string) => number, boolean>\ntype Result1 = (a: number, b: string, x: boolean) => number\n\ntype Case2 = AppendArgument<() => void, undefined>\ntype Result2 = (x: undefined) => void\n\ntype Cases = [\n  Expect<Equal<Case1, Result1>>,\n  Expect<Equal<Case2, Result2>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}