{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/2793-medium-mutable/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/2793-medium-mutable/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>Mutable <img src=\"https://img.shields.io/badge/-medium-d9901a\" alt=\"medium\"/> <img src=\"https://img.shields.io/badge/-%23readonly-999\" alt=\"#readonly\"/> <img src=\"https://img.shields.io/badge/-%23object--keys-999\" alt=\"#object-keys\"/></h1><blockquote><p>by jiangshan <a href=\"https://github.com/jiangshanmeta\" target=\"_blank\">@jiangshanmeta</a></p></blockquote><p><a href=\"https://tsch.js.org/2793/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement the generic ```Mutable<T>``` which makes all properties in ```T``` mutable (not readonly).\r",
                                "\r",
                                "For example\r",
                                "\r",
                                "```typescript\r",
                                "interface Todo {\r",
                                "  readonly title: string\r",
                                "  readonly description: string\r",
                                "  readonly completed: boolean\r",
                                "}\r",
                                "\r",
                                "type MutableTodo = Mutable<T> // { title: string; description: string; completed: boolean; }\r",
                                "\r",
                                "```",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/2793/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/2793/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/2793-medium-mutable/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype Mutable<Type> = { -readonly [Key in keyof Type]: Type[Key]}\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ninterface Todo1 {\n  title: string\n  description: string\n  completed: boolean\n  meta: {\n    author: string\n  }\n}\n\ntype Cases = [\n  Expect<Equal<Mutable<Readonly<Todo1>>, Todo1>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}