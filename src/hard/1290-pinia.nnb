{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/1290-hard-pinia/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/1290-hard-pinia/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>Pinia <img src=\"https://img.shields.io/badge/-hard-de3d37\" alt=\"hard\"/> <img src=\"https://img.shields.io/badge/-%23this-999\" alt=\"#this\"/> <img src=\"https://img.shields.io/badge/-%23vue-999\" alt=\"#vue\"/></h1><blockquote><p>by Pig Fang <a href=\"https://github.com/g-plane\" target=\"_blank\">@g-plane</a></p></blockquote><p><a href=\"https://tsch.js.org/1290/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "Create a type-level function whose types is similar to [Pinia](https://github.com/posva/pinia) library. You don't need to implement function actually, just adding types.\r",
                                "\r",
                                "### Overview\r",
                                "\r",
                                "This function receive only one parameter whose type is an object. The object contains 4 properties:\r",
                                "\r",
                                "- `id` - just a string (required)\r",
                                "- `state` - a function which will return an object as store's state (required)\r",
                                "- `getters` - an object with methods which is similar to Vue's computed values or Vuex's getters, and details are below (optional)\r",
                                "- `actions` - an object with methods which can do side effects and mutate state, and details are below (optional)\r",
                                "\r",
                                "### Getters\r",
                                "\r",
                                "When you define a store like this:\r",
                                "\r",
                                "```typescript\r",
                                "const store = defineStore({\r",
                                "  // ...other required fields\r",
                                "  getters: {\r",
                                "    getSomething() {\r",
                                "      return 'xxx'\r",
                                "    }\r",
                                "  }\r",
                                "})\r",
                                "```\r",
                                "\r",
                                "And you should use it like this:\r",
                                "\r",
                                "```typescript\r",
                                "store.getSomething\r",
                                "```\r",
                                "\r",
                                "instead of:\r",
                                "\r",
                                "```typescript\r",
                                "store.getSomething()  // error\r",
                                "```\r",
                                "\r",
                                "Additionally, getters can access state and/or other getters via `this`, but state is read-only.\r",
                                "\r",
                                "### Actions\r",
                                "\r",
                                "When you define a store like this:\r",
                                "\r",
                                "```typescript\r",
                                "const store = defineStore({\r",
                                "  // ...other required fields\r",
                                "  actions: {\r",
                                "    doSideEffect() {\r",
                                "      this.xxx = 'xxx'\r",
                                "      return 'ok'\r",
                                "    }\r",
                                "  }\r",
                                "})\r",
                                "```\r",
                                "\r",
                                "Using it is just to call it:\r",
                                "\r",
                                "```typescript\r",
                                "const returnValue = store.doSideEffect()\r",
                                "```\r",
                                "\r",
                                "Actions can return any value or return nothing, and it can receive any number of parameters with different types.\r",
                                "Parameters types and return type can't be lost, which means type-checking must be available at call side.\r",
                                "\r",
                                "State can be accessed and mutated via `this`. Getters can be accessed via `this` but they're read-only.",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/1290/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/1290/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/1290-hard-pinia/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\n// Submission: https://github.com/type-challenges/type-challenges/issues/3720\ndeclare function defineStore<\n  State,\n  Getters,\n  Actions,\n  DefinedGetters extends Readonly<{\n    [Key in keyof Getters]\n      : Getters[Key] extends ()=> infer Value ? Value : never\n  }>,\n  DefinedStore extends State & DefinedGetters & Actions,\n>(store: {\n  id: string,\n  state: () => State,\n  getters: Getters & ThisType<Readonly<DefinedStore>>,\n  actions: Actions & ThisType<DefinedStore>,\n}): DefinedStore\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\nconst store = defineStore({\n  id: '',\n  state: () => ({\n    num: 0,\n    str: '',\n  }),\n  getters: {\n    stringifiedNum() {\n      // @ts-expect-error\n      this.num += 1\n\n      return this.num.toString()\n    },\n    parsedNum() {\n      return parseInt(this.stringifiedNum)\n    },\n  },\n  actions: {\n    init() {\n      this.reset()\n      this.increment()\n    },\n    increment(step = 1) {\n      this.num += step\n    },\n    reset() {\n      this.num = 0\n\n      // @ts-expect-error\n      this.parsedNum = 0\n\n      return true\n    },\n    setNum(value: number) {\n      this.num = value\n    },\n  },\n})\n\n// @ts-expect-error\nstore.nopeStateProp\n// @ts-expect-error\nstore.nopeGetter\n// @ts-expect-error\nstore.stringifiedNum()\nstore.init()\n// @ts-expect-error\nstore.init(0)\nstore.increment()\nstore.increment(2)\n// @ts-expect-error\nstore.setNum()\n// @ts-expect-error\nstore.setNum('3')\nstore.setNum(3)\nconst r = store.reset()\n\ntype _tests = [\n  Expect<Equal<typeof store.num, number>>,\n  Expect<Equal<typeof store.str, string>>,\n  Expect<Equal<typeof store.stringifiedNum, string>>,\n  Expect<Equal<typeof store.parsedNum, number>>,\n  Expect<Equal<typeof r, true>>\n]\n"
            ],
            "outputs": []
        }
    ]
}