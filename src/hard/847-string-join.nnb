{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/847-hard-string-join/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/847-hard-string-join/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>String Join <img src=\"https://img.shields.io/badge/-hard-de3d37\" alt=\"hard\"/> </h1><blockquote><p>by Matt Davis <a href=\"https://github.com/tl-matt-davis\" target=\"_blank\">@tl-matt-davis</a></p></blockquote><p><a href=\"https://tsch.js.org/847/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "Create a type-safe string join utility which can be used like so:\r",
                                "\r",
                                "```ts\r",
                                "const hyphenJoiner = join('-')\r",
                                "const result = hyphenJoiner('a', 'b', 'c'); // = 'a-b-c'\r",
                                "```\r",
                                "\r",
                                "Or alternatively:\r",
                                "```ts\r",
                                "join('#')('a', 'b', 'c') // = 'a#b#c'\r",
                                "```\r",
                                "\r",
                                "When we pass an empty delimiter (i.e '') to join, we should concat the strings as they are, i.e: \r",
                                "```ts\r",
                                "join('')('a', 'b', 'c') // = 'abc'\r",
                                "```\r",
                                "\r",
                                "When only one item is passed, we should get back the original item (without any delimiter added):\r",
                                "```ts\r",
                                "join('-')('a') // = 'a'\r",
                                "```",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/847/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/847/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/847-hard-string-join/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\n// Submission: https://github.com/type-challenges/type-challenges/issues/3718\ntype Join<\n  Substrings,\n  Delimiter extends string,\n> = Substrings extends [infer Current, ...infer Rest]\n  ? Rest extends []\n    ? Current\n    : `${Current & string}${Delimiter}${Join<Rest, Delimiter>}`\n  : ''\n\ndeclare function join<\n  Delimiter extends string\n>(delimiter: Delimiter): <\n  Substrings extends ReadonlyArray<string>\n>(...substrings: Substrings)=> Join<Substrings, Delimiter>\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\n// Edge cases\nconst noCharsOutput = join('-')();\nconst oneCharOutput = join('-')('a');\nconst noDelimiterOutput = join('')('a', 'b', 'c');\n\n// Regular cases\nconst hyphenOutput = join('-')('a', 'b', 'c');\nconst hashOutput = join('#')('a', 'b', 'c');\nconst twoCharOutput = join('-')('a', 'b');\nconst longOutput = join('-')('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');\n\ntype Cases = [\n    Expect<Equal<typeof noCharsOutput, ''>>,\n    Expect<Equal<typeof oneCharOutput, 'a'>>,\n    Expect<Equal<typeof noDelimiterOutput, 'abc'>>,\n    Expect<Equal<typeof twoCharOutput, 'a-b'>>,\n    Expect<Equal<typeof hyphenOutput, 'a-b-c'>>,\n    Expect<Equal<typeof hashOutput, 'a#b#c'>>,\n    Expect<Equal<typeof longOutput, 'a-b-c-d-e-f-g-h'>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}