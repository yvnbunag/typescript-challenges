{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/545-hard-printf/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/545-hard-printf/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>printf <img src=\"https://img.shields.io/badge/-hard-de3d37\" alt=\"hard\"/> <img src=\"https://img.shields.io/badge/-%23template--literal-999\" alt=\"#template-literal\"/></h1><blockquote><p>by null <a href=\"https://github.com/BestMaster-YS\" target=\"_blank\">@BestMaster-YS</a></p></blockquote><p><a href=\"https://tsch.js.org/545/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> &nbsp;&nbsp;&nbsp;<a href=\"./README.ja.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E6%97%A5%E6%9C%AC%E8%AA%9E-gray\" alt=\"日本語\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement `Format<T extends string>` generic.\r",
                                "\r",
                                "For example,\r",
                                "\r",
                                "```ts\r",
                                "type FormatCase1 = Format<\"%sabc\"> // FormatCase1 : string => string\r",
                                "type FormatCase2 = Format<\"%s%dabc\"> // FormatCase2 : string => number => string\r",
                                "type FormatCase3 = Format<\"sdabc\"> // FormatCase3 :  string\r",
                                "type FormatCase4 = Format<\"sd%abc\"> // FormatCase4 :  string\r",
                                "```",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/545/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/545/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/545-hard-printf/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\ntype ControlsMap = {\n  s: string,\n  d: number,\n}\ntype Format<\n  Type extends string\n> = Type extends `${string}%${infer Control}${infer Rest}`\n  ? Control extends keyof ControlsMap\n    ? (arg: ControlsMap[Control]) => Format<Rest>\n    : never\n  : string\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect } from '@type-challenges/utils'\n\ntype Cases = [\n  Expect<Equal<Format<'abc'>, string>>,\n  Expect<Equal<Format<'a%sbc'>, (s1: string) => string>>,\n  Expect<Equal<Format<'a%dbc'>, (d1: number) => string>>,\n  Expect<Equal<Format<'a%dbc%s'>, (d1: number) => (s1: string) => string>>\n]\n"
            ],
            "outputs": []
        }
    ]
}