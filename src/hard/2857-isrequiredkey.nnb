{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/2857-hard-isrequiredkey/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/2857-hard-isrequiredkey/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>IsRequiredKey <img src=\"https://img.shields.io/badge/-hard-de3d37\" alt=\"hard\"/> <img src=\"https://img.shields.io/badge/-%23utils-999\" alt=\"#utils\"/></h1><blockquote><p>by jiangshan <a href=\"https://github.com/jiangshanmeta\" target=\"_blank\">@jiangshanmeta</a></p></blockquote><p><a href=\"https://tsch.js.org/2857/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement a generic ```IsRequiredKey<T, K>```  that return whether ```K``` are required keys of ```T``` .\r",
                                "\r",
                                "For example\r",
                                "\r",
                                "```typescript\r",
                                "type A = IsRequiredKey<{ a: number, b?: string },'a'> // true\r",
                                "type B = IsRequiredKey<{ a: number, b?: string },'b'> // false\r",
                                "type C = IsRequiredKey<{ a: number, b?: string },'b' | 'a'> // false\r",
                                "```\r",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/2857/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/2857/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/2857-hard-isrequiredkey/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\n// Submission: https://github.com/type-challenges/type-challenges/issues/3732\ntype IsRequiredKey<\n  Type,\n  Keys extends keyof Type\n> = Pick<Type, Keys> extends Required<Pick<Type, Keys>>\n  ? true\n  : false\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, Expect, ExpectFalse, NotEqual } from '@type-challenges/utils'\n\ntype Cases = [\n    Expect<Equal<IsRequiredKey<{ a: number, b?: string },'a'>, true>>,\n    Expect<Equal<IsRequiredKey<{ a: number, b?: string },'b'>, false>>,\n    Expect<Equal<IsRequiredKey<{ a: number, b?: string },'b' | 'a'>, false>>,\n]\n"
            ],
            "outputs": []
        }
    ]
}