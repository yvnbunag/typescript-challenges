{
    "cells": [
        {
            "language": "typescript",
            "source": [
                "const axios = require('axios')\nconst { markdown }  = require('node-kernel').display\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "const challengeReadmeLink = 'https://github.com/type-challenges/type-challenges/blob/master/questions/651-hard-length-of-string-2/README.md'\nconst challengeRawReadmeLink = 'https://raw.githubusercontent.com/type-challenges/type-challenges/master/questions/651-hard-length-of-string-2/README.md'\nconst readme = axios.get(challengeRawReadmeLink)\n\nreadme\n  .then(({ data }) => markdown(data))\n  .then(() => markdown(`[Go to repository](${challengeReadmeLink})`))\n"
            ],
            "outputs": [
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "<!--info-header-start--><h1>Length of String 2 <img src=\"https://img.shields.io/badge/-hard-de3d37\" alt=\"hard\"/> <img src=\"https://img.shields.io/badge/-%23template--literal-999\" alt=\"#template-literal\"/></h1><blockquote><p>by null <a href=\"https://github.com/uid11\" target=\"_blank\">@uid11</a></p></blockquote><p><a href=\"https://tsch.js.org/651/play\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Take%20the%20Challenge-3178c6?logo=typescript&logoColor=white\" alt=\"Take the Challenge\"/></a> &nbsp;&nbsp;&nbsp;<a href=\"./README.ja.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-%E6%97%A5%E6%9C%AC%E8%AA%9E-gray\" alt=\"日本語\"/></a> </p><!--info-header-end-->",
                                "",
                                "Implement a type `LengthOfString<S>` that calculates the length of the template string (as in [298 - Length of String](https://tsch.js.org/298)):\r",
                                "\r",
                                "```ts\r",
                                "type T0 = LengthOfString<\"foo\"> // 3\r",
                                "```\r",
                                "\r",
                                "The type must support strings several hundred characters long (the usual recursive calculation of the string length is limited by the depth of recursive function calls in TS, that is, it supports strings up to about 45 characters long).",
                                "",
                                "",
                                "<!--info-footer-start--><br><a href=\"../../README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Back-grey\" alt=\"Back\"/></a> <a href=\"https://tsch.js.org/651/answer\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Share%20your%20Solutions-teal\" alt=\"Share your Solutions\"/></a> <a href=\"https://tsch.js.org/651/solutions\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-Check%20out%20Solutions-de5a77?logo=awesome-lists&logoColor=white\" alt=\"Check out Solutions\"/></a> <hr><h3>Related Challenges</h3><a href=\"https://github.com/type-challenges/type-challenges/blob/master/questions/298-medium-length-of-string/README.md\" target=\"_blank\"><img src=\"https://img.shields.io/badge/-298%E3%83%BBLength%20of%20String-d9901a\" alt=\"298・Length of String\"/></a> <!--info-footer-end-->"
                            ]
                        }
                    ]
                },
                {
                    "items": [
                        {
                            "mime": "text/markdown",
                            "value": [
                                "[Go to repository](https://github.com/type-challenges/type-challenges/blob/master/questions/651-hard-length-of-string-2/README.md)"
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Your Code Here _____________ */\n// Submission: https://github.com/type-challenges/type-challenges/issues/3705\ntype s = string\ntype LengthOfString<\n  Type extends string,\n  Counter extends ReadonlyArray<unknown> = []\n> = Type extends `${s}${s}${s}${s}${s}${s}${s}${s}${s}${s}${infer PastTen}`\n  ? LengthOfString<PastTen, [...Counter, s, s, s, s, s, s, s, s, s, s]>\n  : Type extends `${s}${infer PastOne}`\n    ? LengthOfString<PastOne,[...Counter, s]>\n    : Counter['length']\n"
            ],
            "outputs": []
        },
        {
            "language": "typescript",
            "source": [
                "/* _____________ Test Cases _____________ */\nimport type { Equal, IsTrue } from '@type-challenges/utils'\n\ntype Cases = [\n  IsTrue<Equal<LengthOfString<\"\">, 0>>,\n  IsTrue<Equal<LengthOfString<\"1\">, 1>>,\n  IsTrue<Equal<LengthOfString<\"12\">, 2>>,\n  IsTrue<Equal<LengthOfString<\"123\">, 3>>,\n  IsTrue<Equal<LengthOfString<\"1234\">, 4>>,\n  IsTrue<Equal<LengthOfString<\"12345\">, 5>>,\n  IsTrue<Equal<LengthOfString<\"123456\">, 6>>,\n  IsTrue<Equal<LengthOfString<\"1234567\">, 7>>,\n  IsTrue<Equal<LengthOfString<\"12345678\">, 8>>,\n  IsTrue<Equal<LengthOfString<\"123456789\">, 9>>,\n  IsTrue<Equal<LengthOfString<\"1234567890\">, 10>>,\n  IsTrue<Equal<LengthOfString<\"12345678901\">, 11>>,\n  IsTrue<Equal<LengthOfString<\"123456789012\">, 12>>,\n  IsTrue<Equal<LengthOfString<\"1234567890123\">, 13>>,\n  IsTrue<Equal<LengthOfString<\"12345678901234\">, 14>>,\n  IsTrue<Equal<LengthOfString<\"123456789012345\">, 15>>,\n  IsTrue<Equal<LengthOfString<\"1234567890123456\">, 16>>,\n  IsTrue<Equal<LengthOfString<\"12345678901234567\">, 17>>,\n  IsTrue<Equal<LengthOfString<\"123456789012345678\">, 18>>,\n  IsTrue<Equal<LengthOfString<\"1234567890123456789\">, 19>>,\n  IsTrue<Equal<LengthOfString<\"12345678901234567890\">, 20>>,\n  IsTrue<Equal<LengthOfString<\"123456789012345678901\">, 21>>,\n  IsTrue<Equal<LengthOfString<\"1234567890123456789012\">, 22>>,\n  IsTrue<Equal<LengthOfString<\"12345678901234567890123\">, 23>>,\n  IsTrue<Equal<LengthOfString<\"aaaaaaaaaaaaggggggggggggggggggggkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\">, 272>>,\n];\n"
            ],
            "outputs": []
        }
    ]
}