const { mkdirSync, readFileSync, existsSync, writeFileSync } = require('fs')

const Args = {
  COMMAND: 'command',
  CHALLENGE_LINK: 'challenge-link',
}

function requireArgument(arg) {
  const error = [
    `'${arg}' argument is required. Expected format:'`,
    `<${Args.COMMAND}> <${Args.CHALLENGE_LINK}>`
  ].join('\n')

  throw new Error(error)
}

function extractRawReadmeLink(challengeLink) {
  return challengeLink
    .replace(/https:\/\/github.com/g, 'https://raw.githubusercontent.com')
    .replace(/^(.*)(blob)\/(.*)$/, '$1$3')
}

const [challengeLink] = process.argv.slice(2)

if (!challengeLink) requireArgument(Args.CHALLENGE_LINK)

const [difficulty, slug] = (() => {
  const { repositoryName } = challengeLink
    .match(/^(.*)(questions\/)(?<repositoryName>.*)(\/README.md$)/)
    .groups
  const [id, difficulty, ...name] = repositoryName.split('-')

  return [difficulty, [id, ...name].join('-')]
})()
const [subDirectory, fileName] = (() => {
  const subDirectory = slug.split('/')
  const fileName = `${subDirectory.pop()}.nnb`

  return [subDirectory.join('/') || null, fileName]
})()
const subDirectorySuffix = subDirectory ? `/${subDirectory}` : ''
const relativeFileDirectory = `src/${difficulty}${subDirectorySuffix}`
const relativeFilePath = `${relativeFileDirectory}/${fileName}`
const template = readFileSync('src/template.nnb', 'utf8')
  .replace(/<challenge-readme-link>/g, challengeLink)
  .replace(/<challenge-raw-readme-link>/g, extractRawReadmeLink(challengeLink))


if (existsSync(relativeFilePath)) {
  console.log(`Challenge already exists in ${relativeFilePath}, skipping...`)
  process.exit()
}

mkdirSync(relativeFileDirectory, { recursive: true })
writeFileSync(relativeFilePath, template)

console.log(`Challenge scaffolded in ${relativeFilePath}`)
