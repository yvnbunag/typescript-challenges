# TypeScript Challenges

Collection of TypeScript
[type challenges](https://github.com/type-challenges/type-challenges)
for practice with
[Node.js Notebooks](https://marketplace.visualstudio.com/items?itemName=donjayamanne.typescript-notebook)

## See my submissions!

<details>
  <summary>See <strong>extreme</strong> submissions</summary>

  | Challenge                                                                                                                                 | Difficulty | Submission                                                                            |
  |-------------------------------------------------------------------------------------------------------------------------------------------|------------|---------------------------------------------------------------------------------------|
  | [Sort](https://github.com/type-challenges/type-challenges/blob/master/questions/741-extreme-sort/README.md)                               | Extreme    | [Sort](https://github.com/type-challenges/type-challenges/issues/3773)                |
  | [Tag](https://github.com/type-challenges/type-challenges/blob/master/questions/697-extreme-tag/README.md)                                 | Extreme    | [Tag](https://github.com/type-challenges/type-challenges/issues/3768)                 |
  | [Multiply](https://github.com/type-challenges/type-challenges/blob/master/questions/517-extreme-multiply/README.md)                       | Extreme    | [Multiply](https://github.com/type-challenges/type-challenges/issues/3758)            |
  | [Sum](https://github.com/type-challenges/type-challenges/blob/master/questions/476-extreme-sum/README.md)                                 | Extreme    | [Sum](https://github.com/type-challenges/type-challenges/issues/3749)                 |
  | [Currying 2](https://github.com/type-challenges/type-challenges/blob/master/questions/462-extreme-currying-2/README.md)                   | Extreme    | [Currying 2](https://github.com/type-challenges/type-challenges/issues/3747)          |
  | [Integers Comparator](https://github.com/type-challenges/type-challenges/blob/master/questions/274-extreme-integers-comparator/README.md) | Extreme    | [Integers Comparator](https://github.com/type-challenges/type-challenges/issues/3741) |
  | [Query String Parser](https://github.com/type-challenges/type-challenges/blob/master/questions/151-extreme-query-string-parser/README.md) | Extreme    | [Query String Parser](https://github.com/type-challenges/type-challenges/issues/3735) |
  | [Get Readonly Keys](https://github.com/type-challenges/type-challenges/blob/master/questions/5-extreme-readonly-keys/README.md)           | Extreme    | [Get Readonly Keys](https://github.com/type-challenges/type-challenges/issues/3734)   |
</details>

<br/>

<details>
  <summary>See <strong>hard</strong> submissions</summary>

| Challenge                                                                                                                            | Difficulty | Submission                                                                           |
|--------------------------------------------------------------------------------------------------------------------------------------|------------|--------------------------------------------------------------------------------------|
| [ObjectFromEntries](https://github.com/type-challenges/type-challenges/blob/master/questions/2949-hard-objectfromentries/README.md)  | Hard       | [ObjectFromEntries](https://github.com/type-challenges/type-challenges/issues/3733)  |
| [IsRequiredKey](https://github.com/type-challenges/type-challenges/blob/master/questions/2857-hard-isrequiredkey/README.md)          | Hard       | [IsRequiredKey](https://github.com/type-challenges/type-challenges/issues/3732)      |
| [Split](https://github.com/type-challenges/type-challenges/blob/master/questions/2822-hard-split/README.md)                          | Hard       | [Split](https://github.com/type-challenges/type-challenges/issues/3731)              |
| [Pinia](https://github.com/type-challenges/type-challenges/blob/master/questions/1290-hard-pinia/README.md)                          | Hard       | [Pinia](https://github.com/type-challenges/type-challenges/issues/3720)              |
| [DeepPick](https://github.com/type-challenges/type-challenges/blob/master/questions/956-hard-deeppick/README.md)                     | Hard       | [DeepPick](https://github.com/type-challenges/type-challenges/issues/3719)           |
| [String Join](https://github.com/type-challenges/type-challenges/blob/master/questions/847-hard-string-join/README.md)               | Hard       | [String Join](https://github.com/type-challenges/type-challenges/issues/3718)        |
| [Length of String 2](https://github.com/type-challenges/type-challenges/blob/master/questions/651-hard-length-of-string-2/README.md) | Hard       | [Length of String 2](https://github.com/type-challenges/type-challenges/issues/3705) |
| [String to Number](https://github.com/type-challenges/type-challenges/blob/master/questions/300-hard-string-to-number/README.md)     | Hard       | [String to Number](https://github.com/type-challenges/type-challenges/issues/3700)   |
| [Vue Basic Props](https://github.com/type-challenges/type-challenges/blob/master/questions/213-hard-vue-basic-props/README.md)       | Hard       | [Vue Basic Props](https://github.com/type-challenges/type-challenges/issues/3699)    |
| [C-printf Parser](https://github.com/type-challenges/type-challenges/blob/master/questions/147-hard-c-printf-parser/README.md)       | Hard       | [C-printf Parser](https://github.com/type-challenges/type-challenges/issues/3698)    |
| [Capitalize Words](https://github.com/type-challenges/type-challenges/blob/master/questions/112-hard-capitalizewords/README.md)      | Hard       | [Capitalize Words](https://github.com/type-challenges/type-challenges/issues/3684)   |
| [Optional Keys](https://github.com/type-challenges/type-challenges/blob/master/questions/90-hard-optional-keys/README.md)            | Hard       | [Optional Keys](https://github.com/type-challenges/type-challenges/issues/3683)      |
| [Get Optional](https://github.com/type-challenges/type-challenges/blob/master/questions/59-hard-get-optional/README.md)              | Hard       | [Get Optional](https://github.com/type-challenges/type-challenges/issues/3682)       |
| [Get Required](https://github.com/type-challenges/type-challenges/blob/master/questions/57-hard-get-required/README.md)              | Hard       | [Get Required](https://github.com/type-challenges/type-challenges/issues/3672)       |
</details>

<br/>

<details>
  <summary>See <strong>medium</strong> submissions</summary>

| Challenge                                                                                                                       | Difficulty | Submission                                                                       |
|---------------------------------------------------------------------------------------------------------------------------------|------------|----------------------------------------------------------------------------------|
| [Flip Arguments](https://github.com/type-challenges/type-challenges/blob/master/questions/3196-medium-flip-arguments/README.md) | Medium     | [Flip Arguments](https://github.com/type-challenges/type-challenges/issues/3657) |
| [ObjectEntries](https://github.com/type-challenges/type-challenges/blob/master/questions/2946-medium-objectentries/README.md)   | Medium     | [ObjectEntries](https://github.com/type-challenges/type-challenges/issues/3656)  |
| [RequiredBykeys](https://github.com/type-challenges/type-challenges/blob/master/questions/2759-medium-requiredbykeys/README.md) | Medium     | [RequiredBykeys](https://github.com/type-challenges/type-challenges/issues/3635) |
</details>


<br/>

## Requirements

1. [VSCode](https://code.visualstudio.com/) version 1.60.1 or higher
2. [Node.js Notebook](https://marketplace.visualstudio.com/items?itemName=donjayamanne.typescript-notebook)
  extension version 2.03 or higher plugged into VSCode
3. [Node.js](https://nodejs.org/) version 16.9.1 or higher
4. [Yarn](https://classic.yarnpkg.com/lang/en/) version 1.22.11 or higher, lower
  than version 2

## Setup

1. Clone the repository

```sh
git@gitlab.com:yvnbunag/typescript-challenges.git
```

2. Install dependencies

```sh
cd typescript-challenges/
yarn
```

## Usage

### Creating a challenge

1. Pick a challenge from the
  [type challenges](https://github.com/type-challenges/type-challenges)
  repository

2. Run scaffold script

```sh
yarn scaffold <challenge-link>
```

3. Run all cells to fetch and render challenge readme
4. Commit challenge readme snapshot to repository

![creating-a-challenge](documentation/creating-a-challenge.gif)

### Taking the challenge

1. Open the `take challenge` link
2. Copy the code and tests from the TypeScript sandbox
3. Complete the challenge by eliminating TypeScript intellisense errors

![taking-the-challenge](documentation/taking-the-challenge.gif)
